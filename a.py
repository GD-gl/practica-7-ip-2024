# 1.1
def pertenece_1(s: list[int], e: int) -> bool:
    return s.count(e) > 0

def pertenece_2(s: list[int], e: int) -> bool:
    for el in s:
        if el == e:
            return True
    return False

def pertenece_3(s: list[int], e: int) -> bool:
    while len(s) > 0:
        if s.pop() == e:
            return True
    return False

print(pertenece_1([1, 2, 3], 1))
print(pertenece_1([1, 2, 3], 4))
print(pertenece_2([1, 2, 3], 1))
print(pertenece_2([1, 2, 3], 4))
print(pertenece_3([1, 2, 3], 1))
print(pertenece_3([1, 2, 3], 4))

# 1.2
def divide_a_todos(s: list[int], e: int) -> bool:
    for elemento in s:
        if elemento % e != 0:
            return False
    return True

print(divide_a_todos([1, 2, 3, 4], 1))
print(divide_a_todos([2, 4, 6, 8], 2))
print(divide_a_todos([3, 6, 9, 15], 3))
print(divide_a_todos([3, 6, 9, 15], 2))

# 1.3
def suma_total(s: list[int]) -> int:
    x = 0
    for e in s:
        x += e
    return x


print(suma_total([x for x in range(1, 101)]))

# 1.4
def ordenados(s: list[int]) -> bool:
    for i in range(0, len(s) - 1):
        if s[i] >= s[i + 1]:
            return False
    return True

print(ordenados([1, 2, 3, 4]))
print(ordenados([1, 5, 7, 10]))
print(ordenados([1, 1, 2, 3]))
print(ordenados([1, -1, 3, 5]))

# 1.5
def palabra_len_gt_7(p: list[str]) -> bool:
    for palabra in p:
        if len(palabra) > 7:
            return True
    return False

print(palabra_len_gt_7(["hola", "que", "tal"]))
print(palabra_len_gt_7(["mondongo"]))

# 1.6
def palindromo(p: str) -> bool:
    for i in range(0, len(p)):
        if p[i] != p[len(p) - 1 - i]:
            return False
    return True

print(palindromo("hola"))
print(palindromo("neuquen"))

# 1.7
def fortaleza_contra(contra: str) -> str:
    
    if len(contra) < 5:
        return "ROJA"
    
    if len(contra) > 8:
        if not contra.isupper() and not contra.islower():
            if not contra.isalpha() and not contra.isnumeric():
                return "VERDE"
    
    return "AMARILLA"

print(fortaleza_contra("hola"))
print(fortaleza_contra("holaquetal"))
print(fortaleza_contra("HolaQu3Tal"))

# 1.8
def saldo_actual(historial: list[tuple[str, int]]) -> int:
    saldo = 0
    for transaccion in historial:
        match transaccion[0]:
            case "I":
                saldo += transaccion[1]
            case "R":
                saldo -= transaccion[1]
            case _:
                raise "?"
    return saldo

print(saldo_actual([("I", 2000), ("R", 20),("R", 1000),("I", 300)]))

# 1.9
def tres_vocales_distintas(palabra: str) -> bool:
    vocales = []
    for letra in palabra:
        letra_lower = letra.lower()
        match letra_lower:
            case 'a' | 'e' | 'i' | 'o' | 'u':
                if vocales.count(letra_lower) == 0:
                    vocales.append(letra_lower)
    return len(vocales) >= 3

print(tres_vocales_distintas("hola"))
print(tres_vocales_distintas("aaaaaaboooooiiii"))
print(tres_vocales_distintas("aaaaa"))
print(tres_vocales_distintas("choclo"))
print(tres_vocales_distintas("aprendi"))

# 2.1
def anular_pares(numeros: list[int]):
    for i in range(0, len(numeros)):
        if numeros[i] % 2 == 0:
            numeros[i] = 0

numeros = [x for x in range(1, 21)]
anular_pares(numeros)
print(numeros)

# 2.2
def anular_pares_ln(numeros: list[int]):
    lista_nueva = []
    for i in range(0, len(numeros)):
        if numeros[i] % 2 == 0:
            lista_nueva.append(0)
        else:
            lista_nueva.append(numeros[i])
    return lista_nueva

print(anular_pares_ln([x for x in range(1, 21)]))

# 2.3
def sacar_vocales(texto: str) -> str:
    resultado = ""
    for letra in texto:
        if ['a', 'e', 'i', 'o', 'u'].count(letra.lower()) == 0:
            resultado += letra
    return resultado

print(sacar_vocales("hola que tal, todo bien?"))

# 2.4
def reemplaza_vocales(texto: str) -> str:
    resultado = ""
    for letra in texto:
        if ['a', 'e', 'i', 'o', 'u'].count(letra.lower()) == 0:
            resultado += letra
        else:
            resultado += "_"
    return resultado

print(reemplaza_vocales("hola que tal, todo bien?"))

# 2.5
def da_vuelta_str(s: str) -> str:
    res = ""
    for i in range(len(s) - 1, -1, -1):
        res += s[i]
    return res

print(da_vuelta_str("hola que tal"))

# 2.6
def eliminar_repetidos(s: str) -> str:
    elementos_encontrados = []
    resultado = ""
    for letra in s:
        if elementos_encontrados.count(letra) == 0:
            elementos_encontrados.append(letra)
            resultado += letra
    return resultado

print(eliminar_repetidos("hola que tal, todo bien? que bien che! aca tranqui jajaja"))

# 3
def aprobado(notas: list[int]) -> int:
    
    promedio = float(sum(notas)) / float(len(notas))
    notas_mayores_o_iguales_a_4 = True
    
    for nota in notas:
        if nota < 4:
            notas_mayores_o_iguales_a_4 = False
            break

    if notas_mayores_o_iguales_a_4 and promedio >= 7.0:
        return 1
    elif notas_mayores_o_iguales_a_4 and promedio >= 4: # ya se que promedio >= 7.0
        return 2
    return 3

print(aprobado([1, 2, 2, 7, 3, 5]))
print(aprobado([4, 4, 6, 7, 5, 6]))
print(aprobado([10, 5, 8, 7, 9]))

# 4 - input
# 4.1
def pedir_estudiantes() -> list[str]:
    lista = []
    while True:
        elemento = input("(4.1) Introduzca estudiante (o listo): ")
        if elemento == "listo":
            break
        lista.append(elemento)
    return lista

# print(pedir_estudiantes())

# 4.2
def historial_monedero() -> list[tuple[str, int]]:
    
    lista = []
    while True:
        operacion = input("(4.2) Operación? ([C]argar, [D]escontar, [X] finalizar): ")
        if operacion != "C" and operacion != "D":
            if operacion == "X":
                break
            else:
                print("Operación incorrecta")
                continue
    
        monto = int(input("(4.2) Introduzca monto: "))
        lista.append([operacion, monto])

    return lista

# print(historial_monedero())

# 4.3
from random import randint

def es_figura(x: int) -> bool:
    return x == 10 or x == 11 or x == 12

def num_random() -> int:
    x = 8
    while [8, 9].count(x) > 0:
        x = randint(0, 12)
    return x

def puntos(obtenidos: list[int]) -> float:
    puntos = 0.0
    for obtenido in obtenidos:
        if es_figura(obtenido):
            puntos += 0.5
        else:
            puntos += float(obtenido)
    return puntos

def juego_7m():
    
    numero = num_random()
    obtenidos = [numero]
    # print(f"numero = {numero}")
    
    while not (input("Desea plantarse? [s/N]: ").lower() == "s"):
       
        numero = num_random()
        obtenidos.append(numero)
        # print(f"numero = {numero}")
        
        pts = puntos(obtenidos)
        
        if pts > 7.5:
            print(f"{pts} > 7.5")
            break

    print("Puntos:", puntos(obtenidos))
    print("Historial de cartas:", obtenidos)
    
# juego_7m()

# 5.1
def pertenece_a_cada_uno_version_1(s: list[list[int]], e: int, res: list[bool]):
    for i in range(0, len(res)):
        res[i] = pertenece_1(s[i], e)

res_prueba = [False, False, False]
pertenece_a_cada_uno_version_1([[10, 20, 30], [1, 2, 10], [3, 2, 5]], 10, res_prueba)
print(res_prueba)

# 5.2
# lo que cambia es que no depende del length del res dado
def pertenece_a_cada_uno_version_2(s: list[list[int]], e: int, res: list[bool]):
    
    # limpio res
    res.clear()
    for _ in s:
        res.append(False)
    
    for i in range(0, len(res)):
        res[i] = pertenece_1(s[i], e)

res_prueba = []
pertenece_a_cada_uno_version_2([[10, 20, 30], [1, 2, 10], [3, 2, 5]], 10, res_prueba)
print(res_prueba)

# 5.3
def es_matriz(s: list[list[int]]):
    
    if len(s) < 1:
        return False

    if len(s[0]) < 1:
        return False
    
    for fila in s:
        if not (len(fila) == len(s[0])):
            return False
        
    return True

print(es_matriz([[1, 2, 3], [4, 5, 6]]))
print(es_matriz([[1, 2, 3], [4]]))

# 5.4
# nadie me asegura que |s| >= |res| pero bueno, malisimo
def filas_ordenadas(m: list[list[int]], res: list[bool]):
    for i in range(0, len(res)):
        res[i] = ordenados(m[i])

res_v = [False, False]
filas_ordenadas([[1, 2, 3], [4, 5, 6]], res_v)
print(res_v)
res_v = [False, False]
filas_ordenadas([[1, 2, 3], [5, 4, 6]], res_v)
print(res_v)
res_v = [False, False]
filas_ordenadas([[1, 4, 3], [5, 6, 7]], res_v)
print(res_v)

# 5.5
from random import random

# asumo q la matriz tiene al menos 1 fila
def multiplicar_matriz(matriz1: list[list[int]], matriz2: list[list[int]]) -> list[list[int]]:
    
    # hago un res en blanco con la forma de matriz1
    res = []
    for _ in range(0, len(matriz1)):
        fila = []
        for _ in range(0, len(matriz1[0])):
            fila.append(0.0)
        res.append(fila)
    
    for i in range(0, len(res)):
        for j in range(0, len(res[i])):
            suma = 0.0
            for k in range(0, len(res[i])):
                suma += (matriz1[i][k] * matriz2[k][j])
            res[i][j] = suma
    
    return res

def raro(d: int, p: int):
    
    matriz = []
    
    # genero matriz como me pinte
    for _ in range(0, d):
        valores_fila = []
        for _ in range(0, d):
            # uso valores aleatorios de [0, 5)
            valores_fila.append(random() * 5)
        matriz.append(valores_fila)
    
    resultado = matriz.copy()
    for _ in range(0, p):
        # print(resultado)
        resultado = multiplicar_matriz(resultado, matriz)

    return resultado

print(raro(2, 4))